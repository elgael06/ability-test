import os
from fastapi import FastAPI
import pony.orm as pny
from fastapi.middleware.cors import CORSMiddleware

from controllers import TestControllers, PublicsControllers

app = FastAPI()

origins = ["http://192.168.1.6:5173","http://localhost:5173", "http://localhost:3000"]

# config middleware
app.add_middleware(
  # cors
  CORSMiddleware,
  allow_origins=origins,
  allow_credentials=True,
  allow_methods=["*"],
  allow_headers=["*"],
  # others
)


# api routes
app.include_router(TestControllers.router)
app.include_router(PublicsControllers.router)

# turn on debug mode
pny.sql_debug(True)

# port = int(os.environ.get('PORT', 5000))