
from typing import List
from pydantic import BaseModel

class Response(BaseModel):
  message: str
  error: bool= False

class ResultTestId(BaseModel):
  uuid: str
  title: str
  autor: str
  opciones: int
  acientos: int
  total: str
  aprobado: bool

class TotalResultResponce(Response):
  data: ResultTestId

class CreateTestResponse(Response):
  uuid: str
  title: str
  create: str
  options: int

class Respuesta(BaseModel):
  uuid: str
  titulo: str

class Opciones(BaseModel):
  description: str
  uuid: str
  lista_respuestas: List[Respuesta] =[]

class TestById(BaseModel):
  uuid: str
  title: str
  autor: str
  opciones: List[Opciones] = []
  total: int

class TestByIdResponse(Response):
  data: TestById = None

class Title(BaseModel):
  id: str
  title: str

class TitleTestResponse(Response):
  data: List[Title] = []

