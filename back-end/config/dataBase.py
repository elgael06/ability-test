import pony.orm as pny
# database = pny.Database("sqlite",
#                         "test.sqlite",
#                         create_db=True)

database = pny.Database(provider='mysql', host='localhost',
                        user='gael', passwd='1111', db='ability-test')

# # SQLite
# db.bind(provider='sqlite', filename=':sharedmemory:')
# # or
# db.bind(provider='sqlite', filename='database.sqlite', create_db=True)

# # PostgreSQL
# db.bind(provider='postgres', user='', password='', host='', database='')

# # MySQL
# db.bind(provider='mysql', host='', user='', passwd='', db='')

# # Oracle
# db.bind(provider='oracle', user='', password='', dsn='')

# # CockroachDB
# db.bind(provider='cockroach', user='', password='', host='', database='', )