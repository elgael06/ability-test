from datetime import datetime
from email.policy import default
from pony import orm
from config.dataBase import database



class Respuesta(database.Entity):
    uuid = orm.PrimaryKey(str)
    description = orm.Required(str)
    opcion = orm.Required('Opcion', reverse='respuesta_id')


class Opcion(database.Entity):
    uuid = orm.PrimaryKey(str)
    description = orm.Required(str)
    uuid_respuesta = orm.Required(str)
    cuestion = orm.Required('Cuestionario', reverse='optios_cuestion')
    respuesta_id = orm.Set('Respuesta',cascade_delete=True, reverse='opcion')


class Cuestionario(database.Entity):
    uuid = orm.PrimaryKey(str)
    title = orm.Required(str)
    optios_cuestion = orm.Set('Opcion', cascade_delete=True, reverse='cuestion')
    cretate_at = orm.Required(datetime)
    name_creator = orm.Optional(str)


# map the models to the database
# and create the tables, if they don't exist
database.generate_mapping(create_tables=True, check_tables=True)