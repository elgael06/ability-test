from typing import List
from pony import orm
from datetime import datetime
import  uuid
from models import Test
from interfaces.dto.create_test import CreateTestDTO, ResutlCheckItemDTO
from interfaces.TestResponse import (
    CreateTestResponse,
    TestByIdResponse,
    TitleTestResponse,
    TotalResultResponce,
)

@orm.db_session()
def create_new_test(value: CreateTestDTO) -> CreateTestResponse:
    title = value.title
    creator = value.creator
    opciones = value.opciones
    new_test = Test.Cuestionario(
        title=title,
        uuid=str(uuid.uuid1()),
        cretate_at= datetime.now(),
        name_creator = creator,
    )
    for opt in opciones:
        title_option = opt.description
        respuestas = []

        option = Test.Opcion(
            uuid=str(uuid.uuid1()),
            description = title_option,
            cuestion = new_test,
            uuid_respuesta = str(uuid.uuid1()),
        )

        for res in opt.Respuestas:
            id_res = str(uuid.uuid1())
            if res.status:
                option.uuid_respuesta = id_res

            respuestas.append(
                Test.Respuesta(
                    description = res.description,
                    opcion = option,
                    uuid = id_res,
                )
            )
    # print('id test=>', new_test.uuid)
    return {
        'message': 'guardado correctamente',
        'uuid': new_test.uuid,
        'title': new_test.title,
        'create':new_test.cretate_at,
        'options': len(opciones),
    }

@orm.db_session()
def update_test(test_id=0, options = [],):
    return {
        'message': 'guardado correctamente',
        'id': 0,
    }

@orm.db_session()
def get_title_test() -> TitleTestResponse:
    try:
        list_test = orm.select(c for c in Test.Cuestionario) #Test.Cuestionario.get()
        response = []
        # list_test.show()
        # print(list_test.count())
        if list_test.exists() :
            # print('exitst')
            for test in list_test:
                # print('dato')
                # print('test=> ', test.title)
                response.append({
                    'id': test.uuid,
                    'title': test.title
                })
    except:
        return { 'message': 'error al consultar datos!', 'error': True }
    return {'data': response, 'message': 'consulta correcta', 'error': False}

@orm.db_session()
def get_test_id(id: str) -> TestByIdResponse:
    try:
        cuestionario = Test.Cuestionario.get(uuid=id)
        if not cuestionario:
            return {'message': 'cuestionario no encontrado!', 'error': False}
        lista_opciones = []
        opciones = Test.Opcion.select(lambda c: c.cuestion == cuestionario)

        for opcion in opciones:
            respuestas = Test.Respuesta.select(lambda r: r.opcion == opcion)
            lista_respuestas = []

            for respuesta in respuestas:
                lista_respuestas.append({
                    'uuid': respuesta.uuid,
                    'titulo': respuesta.description,
                })
            lista_opciones.append({
                'description': opcion.description,
                'uuid': opcion.uuid,
                'lista_respuestas': lista_respuestas,
            })
        return {
            'data': {
                'uuid': cuestionario.uuid,
                'title': cuestionario.title,
                'autor': cuestionario.name_creator,
                'opciones': lista_opciones,
                'total': len(lista_opciones),
            },
            'message': 'consulta correcta',
            'error': False
        }
    except:
        return { 'message': 'error al consultar datos!', 'error': True }

@orm.db_session()
def get_test_id_result(id, results: List[ResutlCheckItemDTO] = [])-> TotalResultResponce:
    # results
    #      {option_id, respuesta_id}
    try:
        cuestionario = Test.Cuestionario.get(uuid=id)
        if not cuestionario:
            return {'message': 'cuestionario no encontrado!', 'error': False}

        lista_opciones = []
        for result in results:
            opcion = Test.Opcion.select(
                lambda c: result.option_id == c.uuid and c.uuid_respuesta == result.respuesta_id)
            # print('debug', result.respuesta_id)
            if len(opcion) > 0:
                lista_opciones.append(result.option_id)
        total = round((len(lista_opciones) / len(results)) * 100, 2)
        aprobado = total> 60
        return {
            'data': {
                'uuid': cuestionario.uuid,
                'title': cuestionario.title,
                'autor': cuestionario.name_creator,
                'opciones': len(results),
                'acientos': len(lista_opciones),
                'total': "{} %".format(total),
                'aprobado': aprobado,
            },
            'message': 'consulta correcta',
            'error': False
        }
    except:
        return {'message': 'error al consultar datos!', 'error': True}

