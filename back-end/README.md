# ability test

## Caso de uso

Yo como entidad educativa necesito poder evaluar el conocimiento de los nuevos alumnos y así conocer su perfil logrando abordar sus cursos a partir del conocimiento base.

## Requerimientos

Se requiere crear, modificar y eliminar los cursos para evaluar a los nuevos alumnos.
Los cursos deberán ser auto evaluables según la cantidad de respuestas de un cierto resumen de resultado.
 debe ser posible imprimir el resultado.
Debe poder variar entre una o varias respuestas según la necesidad.

## Módulos

- Recursos (altas, bajas y cambios).
- Selección de recurso para alumno.
- Aplicación de recurso al alumno.
- Que las opciones se ordenan aleatoriamente.
- vistas con opción de impresión de resultado recurso.

## Propiedades de recurso

Las opciones del recurso deberá tener los siguientes valores.

- Título de recurso.
- descripción de recurso.
- opciones de respuesta.
si es simple o múltiple.
- indicador de correcta/correctas.
- separación de resultado con respectiva descripción la cantidad de esta puede variar.
  - De 0 a 60, descripción.
  - De 61 a 70, descripción.
  - De 71 a 80, descripción.
  - De 81 a 90, descripción.
  - De 91 a 100, descripción.

## Requerimientos

[pyton](https://www.python.org/)

[vscode](https://code.visualstudio.com/)

[preparativos](https://code.visualstudio.com/docs/python/python-tutorial)

[Flask](https://flask.palletsprojects.com/en/2.1.x/quickstart/)

## Comandos de creacion proyecto

### Crear de entorno virtual

```bash
python3 -m venv .env/
```

### activar env
```bash
windows
.\.env\Scripts\activate.bat
```
```bash
linux/mac
source .\.env\Scripts\activate
```

### installar requerimientos

```bash
 pip install -r  requirements.txt
```

### respaldar requerimientos
```bash
pip freeze > requirements.txt
```

### seleccion de entorno virtual

```bash
python3 -m venv .env/
```


### ejecucion modo desarrollo

```bash
uvicorn main:app --reload --host 0.0.0.0 --port 8000
```
