from fastapi import APIRouter
from typing import Union

router = APIRouter(
  prefix="/api/public",
  tags=["public"],
  # dependencies=[Depends(get_token_header)],
  responses={404: {"description": "Not found 'TEST!'"}},
)

@router.get("/")
def read_root():
  return {"Hello": "World"}


@router.get("/items/{item_id}")
def read_item(item_id: int, q: Union[str, None] = None):
  return {"item_id": item_id, "q": q}
