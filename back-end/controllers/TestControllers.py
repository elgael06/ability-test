from fastapi import APIRouter
from services import TestServices
from interfaces.dto.create_test import CreateTestDTO, ResultTestDTO
from interfaces.TestResponse import (
  CreateTestResponse,
  TestByIdResponse,
  TitleTestResponse,
  TotalResultResponce,
)

# configure route
router = APIRouter(
  prefix="/api/v1/test",
  tags=["test"],
  # dependencies=[Depends(get_token_header)],
  responses={404: {"description": "Not found 'TEST!'"}},
)

# routes
@router.get('/', response_model=TitleTestResponse)
async def get_all_test():
  return TestServices.get_title_test()


@router.get('/{id}', response_model=TestByIdResponse)
async def get_test_by_id(id: str):
  return TestServices.get_test_id(id)


@router.post('/add', response_model=CreateTestResponse)
async def create_test(body: CreateTestDTO):
  return TestServices.create_new_test(body)


@router.post('/check/{id}', response_model=TotalResultResponce)
async def check_test(id: str, test: ResultTestDTO):
  return TestServices.get_test_id_result(id, test.results)
