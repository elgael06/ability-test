import './App.css'
import { Link, Outlet } from 'react-router-dom'
import { Card, Container } from '@nextui-org/react'

function App() {
  return (<main className='App'>
    <Container>
      <Card css={{ 
          // $$cardColor: '$colors$secondary',
          height: 80,
          display: 'flex',
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'space-around',
          color: 'gray !important',
        }}>
        <Link to='/'><b>inicio</b></Link>
        <Link to='/crear'>crear</Link>
        <Link to='/about'>acerca de</Link>
      </Card>
    </Container>
    <Container css={{height: 'calc(100% - 120px)', marginTop: 20 }}>
      <Card css={{height: '100%', width: '100%', display: 'flex', alignItems: 'center' }}>
        <Outlet />
      </Card>
    </Container>
  </main>
  )
}

export default App
