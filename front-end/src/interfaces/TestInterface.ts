
export interface FetchData<t> {
  data: t;
  isLoading: boolean;
  isError:boolean;
}

export interface TestBase {
  error: false;
  message: string;
}

export interface TestData {
  id: string;
  title: string;
}

export interface TestDTO extends TestBase {
  data: TestData[];
}

export interface ContenidoData {
  titulo?: string;
  title?: string;
  uuid: string;
}

export interface respuestaDto {
  option_id: string;
  respuesta_id: string;
}

export interface OpcionesData {
  description: string;
  uuid: string
  lista_respuestas: ContenidoData[];
}

export interface TestFullData extends ContenidoData {
  autor?: string | undefined;
  total: number;
  opciones: OpcionesData[];
}

export interface TestCompleteData extends TestBase {
  data: TestFullData;
}

export interface CreateRespuestas {
  description: string;
  status: boolean;
}
export interface CreateOptions {
  description: string;
  Respuestas: CreateRespuestas[];
}

export interface CrateTestData {
  title: string;
  creator: string;
  opciones: CreateOptions[];
}

export interface ResponseTestData {
  title: string;
  acientos: number;
  opciones: number;
  total: string;
  aprobado: boolean;
}
