import { lazy, Suspense } from "react";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import App from "../App";

const pathsComponentsApp = [
  {uri: '/', Component: lazy(()=> import('../Pages/Home')),},
  {uri: '/course/:idCurso', Component: lazy(()=> import('../Pages/CourseId')),},
  {uri: '/crear', Component: lazy(()=> import('../Pages/CreateTest')),},
]

export default () => {

  return (<BrowserRouter>
    <Suspense fallback={<div>cargando...</div>}>
      <Routes>
        {/* app routes */}
        <Route path="/" element={<App />}>
          {pathsComponentsApp.map(({ uri, Component }, idx) => (<Route key={`app_${idx}`} path={uri} element={<Component />} />))}
          <Route path="/*" element={<h1>error 404</h1>} />
        </Route>
        {/* login routes */}
      </Routes>
    </Suspense>
  </BrowserRouter>);
}