
export interface PropsBtnResp {
  title: string;
  onClick: () => void;
  isSelected: boolean;
};
