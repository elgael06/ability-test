import { Button, Grid } from "@nextui-org/react";

interface BotoneraEventosProps {
  title: string;
  isNext: boolean;
  isPrev: boolean;
  handleNext: () => void;
  handlePrevious: () => void;
} 

const BotoneraEventos = ({
  title,
  isNext,
  isPrev,
  handleNext,
  handlePrevious,
}: BotoneraEventosProps) => {

  return (<Grid.Container gap={2} justify="center">
    <Grid xs>
      <Button
        size={'xs'}
        disabled={isPrev}
        onClick={handlePrevious}
      >{'<'}</Button>
    </Grid>

    <Grid xs>
      <span>{' '}{title}</span>
    </Grid>

    <Grid xs>
      <Button
        size={'xs'}
        disabled={isNext}
        onClick={handleNext}
      >{'>'}</Button>
    </Grid>
  </Grid.Container>);
}

export default BotoneraEventos;