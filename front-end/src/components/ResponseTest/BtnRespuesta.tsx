import { Button } from "@nextui-org/react";
import { PropsBtnResp } from "./interface";

const BtnRespuesta = ({
  title,
  onClick,
  isSelected,
}: PropsBtnResp) => {

  return (<Button
      onClick={onClick}
      style={{
        width: 'calc(100% - 20px)',
        display: 'block',
        margin: 10,
        background: isSelected ? 'blue' : '#1a1a1a80',
      }}>
      {title}
    </Button>);
}


export default BtnRespuesta;