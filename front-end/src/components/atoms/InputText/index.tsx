import { Input } from "@nextui-org/react";
import React from "react";


const styles = {
  height: '30px',
  minWidth: '320px',
  // borderRadius: '10px',
  // border: '3px solid #3434a1',
};

interface Props {
  value: string;
  placeholder?: string;
  label?: string;
  disabled?: boolean;
  clearable?: boolean;
  underlined?: boolean;
  type: React.HTMLInputTypeAttribute | undefined;
  onChange: (value: string) => void;
  style?: object;
}

const InputText = (props: Props) => {

  return (
    <Input
      style={{...styles, ...props.style}}
      type={props.type}
      label={props.label}
      disabled={props.disabled}
      clearable={props.clearable}
      underlined={props.underlined}
      placeholder={props.placeholder}
      onChange={(e) => props.onChange(e.target.value)}
      value={props.value}
    />
  );
}

export default InputText;