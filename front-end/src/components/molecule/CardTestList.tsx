import { Card, Row, Text } from "@nextui-org/react";
import React from "react";
import { Link } from "react-router-dom";
import { TestData } from "../../interfaces/TestInterface";

interface Props {
  item: TestData;
}

const CardTestList = ({item}: Props): React.ReactElement<Props> => {

  return (<Card variant="bordered">
  <Card.Body>
    <Text h3>{item.title}</Text>
  </Card.Body>
  <Card.Footer>
    <Row wrap="wrap" justify="flex-end" align="flex-end">
      <Link to={`/course/${item.id}`}>iniciar</Link>
    </Row>
  </Card.Footer>
</Card>);
}

export default CardTestList;