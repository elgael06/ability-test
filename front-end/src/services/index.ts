import { CrateTestData } from './../interfaces/TestInterface';
import { URL_API } from "../constants";
import { FetchData, ResponseTestData, respuestaDto, TestCompleteData, TestData } from "../interfaces/TestInterface";

export const fetchAllTest = async (): Promise<FetchData<TestData[]>> => {
  try {
    const res = await fetch(`${URL_API}v1/test`);
    const data = await res.json();
    return { data: data.data, isLoading: false, isError: data.error };
  } catch (error) {
    return { data: [], isLoading: false, isError: true }
  };
};

export const getTestId = async (idCurso: string): Promise<FetchData<TestCompleteData | undefined>> => {
  try {
    const res = await fetch(`${URL_API}v1/test/${idCurso}`);
    const data = await res.json();
    return ({ data: data, isError: data.error, isLoading: false });
  } catch (error) {
    return ({ data: undefined, isError: true, isLoading: false });
  }
};

export const checkTestId = async (idCurso: string, results: respuestaDto[]): Promise<FetchData<ResponseTestData | null>> => {
  try {
    const res = await fetch(`${URL_API}v1/test/check/${idCurso}`,
      {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({results}),
      });
    const data = await res.json();
    return ({data: data.data, isLoading: false, isError: data.error });
  } catch (error) {
    return ({data: null, isLoading: false, isError: true });
  }
}

export const createTest =  async (newTest: CrateTestData) => {
  try {
    const res = await fetch(`${URL_API}v1/test/add`,
      {
        method: 'POST',
            headers: {
              'Content-Type': 'application/json'
            },
            body: JSON.stringify({...newTest}),
      });
    const data = await res.json();
    return ({data: data.data, isLoading: false, isError: data.error });
  } catch (error) {
    return ({data: null, isLoading: false, isError: true });
  }
}