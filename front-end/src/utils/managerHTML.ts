import { ResponseTestData } from "../interfaces/TestInterface";

export const createHTML = ({
    title = '',
    acientos = 0,
    opciones = 0,
    total = '0%',
    aprobado = false,
  }: ResponseTestData) => {
    let cadena = `<div>
      <h1>${title}</h1>
      <b>Aciertos </b> ${acientos} de ${opciones}.
      <br/>
      <b>Total: </b> ${total}
      <p><b>Estatus:</b> <u>${aprobado ? 'Aprobado' : 'Reprobado'}</u> .<p> 
    </div>`;
    return cadena;
}
  
export const opemHTML = (content = '') => {
  (window !== null) && window.open('', 'MsgWindow', 'width=400,height=340').document.write(content);
}