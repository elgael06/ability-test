import { Button, Collapse, Grid, Spacer, Switch, Text } from "@nextui-org/react";
import { useState } from "react";
import { useNavigate } from "react-router-dom";
import InputText from "../components/atoms/InputText";
import { CreateOptions, CrateTestData } from "../interfaces/TestInterface";
import { createTest } from "../services";

const initialNuevaOption: CreateOptions = {
  description:'',
  Respuestas: [],
}

const CreateTest = () => {
  let navigate = useNavigate();
  const [titulo, setTitulo] = useState('');
  const [creador, setCreador] = useState('');
  const [opciones, setOpciones] = useState<CreateOptions[]>([]);

  // TODO metods
  const removeOpcion = (idx: number) => () => {
    setOpciones(opciones.filter((v, index) => index !== idx));
  }

  const handleChangeOption = (idx: number) => (value: string) => {
    const newList = [...opciones].map((opt, id) =>{
      if(idx === id) {
        opt.description = value;
      }
      return opt;
    });
    setOpciones(newList);
  }

  const onAgregarOpcion = (idx: number) => () => {
    const newList = [...opciones].map((opt, id) =>{
      if(idx === id) {
        opt.Respuestas.push({
          description: '',
          status: false,
        });
      }
      return opt;
    });
    setOpciones(newList);
  }

  const onchangeOption = (idxRes: number, idx: number) => (value: string) => {
    const newList = [...opciones].map((opt, id) =>{
      const listaOpciones = [...opt.Respuestas];
      if(idxRes === id) {
        const base = listaOpciones[idx];
        listaOpciones[idx] = {
          ...base,
          description: value,
        };
      }
      return {...opt, Respuestas: listaOpciones};
    });
    setOpciones(newList);
  }

  const onchangeOptionStatus = (idxRes: number, idx: number) => (value: boolean) => {
    const newList = [...opciones].map((opt, id) =>{
      const listaOpciones = [...opt.Respuestas];
      if(idxRes === id) {
        listaOpciones.map(e => ({...e, status: false}));
        const base = listaOpciones[idx];
        listaOpciones[idx] = {
          ...base,
          status: value,
        };
      }
      return {...opt, Respuestas: listaOpciones};
    });
    setOpciones(newList);
  }

  const handleSave = () => {
    const dataSave: CrateTestData = {
      title: titulo,
      creator: creador,
      opciones
    };

    console.log(dataSave);
    createTest(dataSave).then((res) => {
      console.log(res);
      navigate('/');
    });
  }

  return (<>
    <h1>Crear Test</h1>
    {/* inputs */}
    <InputText
      onChange={setTitulo}
      value={titulo}
      label="Titulo"
      placeholder="Crear titulo..."
      clearable
      type={'text'}
    />
    <Spacer y={1.5} />
    <InputText
      onChange={setCreador}
      value={creador}
      label="Creador"
      placeholder="Nombre del Creador..."
      clearable
      type={'text'}
    />
    <Spacer y={.5} />
    {/* boton agregar */}
    <Button
      auto
      color="primary"
      onPress={()=> setOpciones([...opciones, {...initialNuevaOption}])}
    >Agregar</Button>
    <Spacer y={1} />
    {/* listado de opciones */}
    <Grid.Container gap={2} css={{width: 500, height: 'calc(100% - 370px)', overflowY: 'scroll'}}>
      <Grid>
      <Collapse.Group splitted>
      {opciones.map((opt, idx) => (
        <Collapse
          shadow
          bordered
          key={`opcion #${idx}`}
          css={{width: 405}}
          title={opt.description || `opcion ${idx + 1}`}
          arrowIcon={<Button
            size={"xs"}
            auto
            bordered
            color={"error"}
            onPress={removeOpcion(idx)}
          >X</Button>}
        >
          {/* descricon de opcion */}
          <InputText
          value={opt.description}
          clearable
          underlined
          type='text'
          onChange={handleChangeOption(idx)}
          label='Descripcion'
          />
          <Spacer y={1} />
          <Text>Opciones</Text>
          <Spacer y={1} />
          {/* listado de respuestas */}
          {
            opt.Respuestas.map((res, index)=> (<Grid.Container key={`res #${index}`}>
              {/* descripcion de respuesta */}
              <InputText
              value={res.description}
              type='text'
              placeholder='descripcion'
              label={`opcion ${index + 1}`}
              onChange={onchangeOption(idx, index)}
              />
              {/* status respuesta */}
              <Switch checked={res.status} size='xs' onChange={e => onchangeOptionStatus(idx, index)(e.target.checked)} />
              <Spacer y={.5} />
            </Grid.Container>)
            )
          }
          <Spacer y={1} />
          {/* agregar opcion */}
          <Button color={"gradient"} onPress={onAgregarOpcion(idx)} auto >Agregar Opcion</Button>
        </Collapse>
      ))}
      </Collapse.Group>
      </Grid>
    </Grid.Container>
    <Spacer y={1} />
    {/* boton guardar */}
    <Button
      color={"success"}
      disabled={(titulo === '' || creador === '')}
      onPress={handleSave}
    >Guardar</Button>
  </>);
}

export default CreateTest;