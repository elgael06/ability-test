import { useEffect, useMemo, useState } from "react";
import InputText from "../components/atoms/InputText";
import CardTestList from "../components/molecule/CardTestList";
import { Loading, Spacer } from "@nextui-org/react";
import { FetchData, TestData } from "../interfaces/TestInterface";
import { fetchAllTest } from "../services";

const Home = () => {
  // estado
  const [data, setData] = useState<FetchData<TestData[]>>({
    data: [],
    isError: false,
    isLoading:false,
  });
  const [filter, setFilter] = useState('');

  // metodos
  const responseData = async () => {
    try {
      const data = await fetchAllTest();
      setData({ ...data });
    } catch (error){
      errorLoad(error);
    };
  };

  const errorLoad = (error:any) => {
    setData({ ...data, isLoading: false, isError: true });
  };

  // eventos de estado
  useEffect(() => {
    setData({ ...data, isLoading: true });
    responseData();
  }, []);

  const listTest = useMemo(() => {
    return data.data?.filter(item => item.title.includes(filter)) || [];
  }, [data, filter]);

  return (<div>
    <h1>Cuestionarios</h1>
    <InputText
      type="text"
      value={filter}
      style={{width: '100%'}}
      placeholder='Search cuestionario...'
      onChange={(e) => setFilter(e)}
    />
    <hr />
    {
      data.isLoading ? <div style={{textAlign: 'center', marginTop: 50}}>
        <Loading loadingCss={{ $$loadingSize: "30px", $$loadingBorder: "3px" }} type="points" />
        </div> :
        data.isError ? <div>Error al cargar!</div> :
          listTest?.map(item => <>
            <CardTestList
              key={item.id}
              item={item}
            />
          <Spacer y={.5} />
          </>)
    }
  </div>);
}

export default Home;
