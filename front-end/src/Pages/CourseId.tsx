import { Button } from "@nextui-org/react";
import { useEffect, useMemo, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import BotoneraEventos from "../components/ResponseTest/BotoneraEventos";
import BtnRespuesta from "../components/ResponseTest/BtnRespuesta";
import { FetchData, respuestaDto, TestFullData } from "../interfaces/TestInterface";
import { checkTestId, getTestId } from "../services";
import { createHTML, opemHTML } from "../utils/managerHTML";

const CourseId = () => {
  const { idCurso = '' } = useParams();
  let navigate = useNavigate();
  const [contenido, setContenido] = useState<TestFullData | undefined>();
  const [value, setValue] = useState<FetchData<undefined>>({
    isError: false,
    isLoading: true,
    data: undefined,
  });
  const [idxOpcion, setIdxOpcion] = useState(1);
  const [selecteds, setSelected] = useState<respuestaDto[]>([]);

  const opcion = useMemo(() => {
    return contenido?.opciones[idxOpcion-1];
  }, [idxOpcion, contenido]);

  const opcionSelected = useMemo(() => {
    return selecteds[idxOpcion-1];
  }, [idxOpcion, selecteds]);

  // metodos
  const responseData = () => {
    getTestId(idCurso).then(res => {
      setValue({
        ...res,
        data: undefined,
      });
      if( res.data && !res.isError)
        setContenido(res.data.data);
    });
  };

  const handleSelected = (id = '') => () => {
    const list = [...selecteds];
    list[idxOpcion - 1] = { option_id: opcion?.uuid || '', respuesta_id: id };
    setSelected([...list]);
  };

  const guardarCurso = () => {
    checkTestId(idCurso, selecteds).then(({ data, isError }) => {
      if (!isError) {
        const str = data ? createHTML(data) : '<h1>Sin datos a mostrar</h1>';
        opemHTML(str);
        navigate('/', { replace: true, });
      } else alert('<h1>Sin datos a mostrar</h1>');
    })
  }

  // eventos de estado
  useEffect(() => {
        responseData();
  }, []);

  return value?.isLoading ? <div>cargando...</div> :
    value?.isError ? <>Error...</> : (<div>
      <h1>{contenido?.title || ''}</h1>

      <p>{opcion?.description}</p>

      {opcion?.lista_respuestas.map(item => <BtnRespuesta
        key={item.uuid}
        onClick={handleSelected(item.uuid)}
        isSelected={opcionSelected?.respuesta_id === item.uuid}
        title={item?.titulo || ''}
      />)}

      <BotoneraEventos
        title={`${idxOpcion} de ${contenido?.total}`}
        isPrev={idxOpcion === 1}
        isNext={idxOpcion === contenido?.total || !opcionSelected}
        handlePrevious={() => setIdxOpcion(idxOpcion - 1)}
        handleNext={() => setIdxOpcion(idxOpcion + 1)}
      />
      <br />

      <Button
        about="boton de guardado..."
        color={'success'}
        size='lg'
        disabled={idxOpcion !== contenido?.total || !opcionSelected}
        onPress={guardarCurso}
      >Guardar</Button>
      <hr />
      <small>Autor: <u>{ contenido?.autor}</u></small>
    </div>);
};

export default CourseId;